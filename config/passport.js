const Mongoose =require('mongoose'); 
const User = require('./../models/userModel');
const LocalStrategy = require('passport-local'). Strategy;
const Bcrypt = require ('bcryptjs');

module.exports = function(passport){
    passport.use(new LocalStrategy(function(username,password,done){
        User.findOne({username: username}, function(err, user){
           //kui juhtub andmebaasiühenduse probleem
            if(err) {
                return done(err);
            }
            if(!user){
                return done(null,false, {message:'Incorrect username'})
            }
            Bcrypt.compare(password, user.password, (err,success)=>{
            if(err){throw err;}
            if(success){
                return done(null, user);
            }else{
                return done(null,false,{message:'Incorrect password'})
            }
        });
    })
    }));

    passport.serializeUser(function(user, done){
        done(null,user._id);
    });
    passport.deserializeUser(function(userid,done){
        User.findById(userid, function(err, user){
            done(err,user);
        });
    });
}