const Express = require("express");
const Router = Express.Router();
const ArticleController = require("./../controllers/articleController");
const UploadController =require('./../controllers/uploadController');
const Article = require("./../models/articleModel");
const auth = require('./../config/auth'); // sellega lisame auth funk. siia faili juurde

/*
Router.get('/create', async (req, res)=>{
    let newArticle = new Article ({
        title: 'drgrghr5',
        content: 'kjsfdlkdmf',
        description: 'nsdfkjsdnflkvcjds',
        author: '5d8de966bb631b32948298e2'

    });
    let result = await newArticle.save();
    res.send(result);
});
 
*/

//TODO:liiguta funktsioon kontrollerisse
//genereerime html dokumendi vahevara abil
Router.get("/create", auth.ensureAuthenticated, ArticleController.article_get_create_form);
Router.route("/:id")
  .get(ArticleController.article_get_single)
  .put(auth.ensureAuthenticated, UploadController.single('articleImage'), ArticleController.article_put_update)
  .delete(auth.ensureAuthenticated, ArticleController.article_delete);
Router.route("/:id/update").get(auth.ensureAuthenticated, ArticleController.article_get_update_form);
Router.route("/")
  .post(auth.ensureAuthenticated, UploadController.single('articleImage'), ArticleController.article_post_create_form)
  .get(ArticleController.article_get_all);
//Router.get('/', ArticleController.article_get_all);
//Router.post('/', ArticleController.article_post_create_form);

// Router.get("/:id", auth.ensureAuthenticated, ArticleController.article_get_single);

module.exports = Router;
