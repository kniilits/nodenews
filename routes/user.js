const Express = require('express');
const Router = Express.Router();
const UserController = require('./../controllers/userController');
const auth = require('./../config/auth'); // sellega lisame auth funk. siia faili juurde


Router.route('/login')
  .get(auth.forwardAuthenticated, UserController.user_get_login_form)
  .post(UserController.user_login_post);

Router.route('/logout')
  .post(auth.ensureAuthenticated, UserController.user_logout_post);

Router.route('/register')
  .post(UserController.user_post_register);

  //TODO: kasutaja profiil leht 

  Router.route('/dashboard')
  .get(auth.ensureAuthenticated, UserController.user_get_dashboard);


  Router.route('/:id')
  .get(UserController.user_get_profile);

  module.exports = Router;


