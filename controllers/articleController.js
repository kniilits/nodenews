const Article = require("./../models/articleModel");
const Mongoose = require("mongoose");
const Moment = require("moment");
Moment.locale("et");

exports.article_get_all = function(req, res) {
  //otsime andmebaasist artikleid
  //Alternatiiv --> Article.find({},null, (err, articles) =>{
  Article.find().populate('author').exec((err, articles) => {
    //lihtne vea kinnipüümine
    console.log(articles);
    if (err) {
      res, send("Bad request");
      throw err;
    }
    //genereerime html dokumendid vaheavara abil
    res.render("articles/all.ejs", {
      title: "latest news",
      data: articles,
      moment: Moment
    });
  });
};

exports.article_get_single = function(req, res) {
  // altern. Article.find({id : req.params.id},null, (err, articles) =>{
  Article.findById(req.params.id).populate('author').exec((err, article) => {
    // console.log(article);
    if (err) {
      res, send("Bad request");
      throw err;
    }
    //genereerime html dokumendid vaheavara abil
    res.render("articles/single.ejs", {
      title: "latest news",
      data: article,
      moment: Moment
    });
  });
};

exports.article_get_create_form = function(req, res) {
  res.render("articles/create.ejs", {
    title: "Create a new Article",
    method: "POST",
    data: {}
  });
};
exports.article_get_update_form = function(req, res) {
  Article.findById(req.params.id).exec((err, article) => {
    //kui artikkle, mida muuta tahetakse ei kuulu sellele kasutajale, saada ta tagasi lehele kust ta tuli
    if(!article.author.equals(req.user._id)){
      backURL = req.header('Referer') || '/';
      res.redirect(backURL);
    }else{
    res.render("articles/create.ejs", {
      title: "Update Article",
      method: "PUT",
      data: article
    });
  }
  });
};

exports.article_post_create_form = function(req, res) {
    //kui pilti pole
    //console.log(req.body);
   // if(!req.file){
      //console.log('No image file was sent: ');
     // res.send({success: false});
    //}
    //else{
 
  let params = {}
  params.title = req.body.title;
  params.description = req.body.description;
  params.content = req.body.content;
  params.author = req.user;
  if (req.file) params.articleImage = req.file.filename;


  if (!params.title || !params.description || !params.content || !params.author) {
    res.send("All fields must be filled");
  } else {
    let newArticle = new Article(params); 
      newArticle.save().then(result => {
      console.log("INSERT: Article - " + result._id);
      res.redirect("/users/dashboard");
    });
  }
//}
}

exports.article_delete = function(req, res) {
  Article.deleteOne({ _id: req.params.id, author: req.user._id }).exec((err, result) => {
    if (err) {
      res.end("Bad request");
      throw err;
    }
    console.log("DELETE: Article - " + req.params.id);
    res.redirect("/users/dashboard");
  });
};

exports.article_put_update = function(req, res) {
  if (Mongoose.Types.ObjectId.isValid(req.params.id)) {
    let params = {};
    params.title = req.body.title;
    params.description = req.body.description;
    params.content = req.body.content;

    Article.findOneAndUpdate({ _id: req.params.id, author: req.user._id }, params).exec(
      (err, result) => {
        if(!result){
          res.status(401);
          req.end('Invalide request 401')
        }else{
        console.log("UPDATE: Article - " + result._id);
        res.redirect("/users/dashboard");
      }
      });
  } else {
    res.send("Invalide article ID");
  }
};
