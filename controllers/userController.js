
const User = require('../models/userModel');
const Article = require('./../models/articleModel');
const Moment = require('moment');
const Bcrypt = require('bcryptjs'); //pisut aeglasem kui bcrypt vahevara,kuid ei nõua pythoni olemasolu
const Passport = require('passport');
const Mongoose = require('mongoose');
Moment.locale('et');

/* callback versioon andmabaasi  objekti päringust
exports.user_get_dashboard = function (req, res) {
    Article.find().exec((err, articles) => {
        res.render('users/dashboard.ejs', {
            title: 'Dashboard',
            data: articles,
            moment: Moment
        });
    });
} */
//soovitakase kasutada asünkroonset lähenemist. ehk promisse põhine lähenemine andmebaasiobjekti päringul. 
exports.user_get_dashboard = async function(req,res) {
    var articles = await Article.find({author: req.user._id}).catch(function(e){
        console.log('mongoose Promise error' + e);
          res.send('Mongoerror - dashboard');
   });
try{
   
    res.render('users/dashboard.ejs', {
      title: 'Dashboard',
      data: articles,
      moment: Moment
    });
  }
      catch(e){
        console.log(e)
    }
}

exports.user_get_login_form = function (req, res) {
    res.render('users/logreg.ejs', {
        title: 'Login'
    });


}

exports.user_get_profile = async function(req,res){
    let userProfile = await User.findById(req.params.id).exec();
    try{
        res.render('users/profile.ejs',{
            title: userProfile.firstName + ' '+ userProfile.lastName +'profile',
            data: userProfile,
            moment: Moment
        });
    }
    catch(e){
        console.log(e);

    }
}

exports.user_login_post = function(req,res,next){
  Passport.authenticate('local', {
    successRedirect: '/users/dashboard',
    failureRedirect: '/users/login'
  })(req,res,next);
}

exports.user_logout_post = function(req,res,next){
  req.logout();
  res.redirect('/');
}

exports.user_post_register = function (req, res) {
    const { username, firstName, lastName, email, password, password2 } = req.body;
    let errors = []; //vead lükatakse massiivi

    //kontrollime kas siendid on täidetud
    if (!username || !firstName || !lastName || !email || !password || !password2) {
        errors.push('Kõik väljad peavad olema täidetud');
    }
    if (password != password2) {
        errors.push('Parool ei vasta parooli kinnitusele');
    }

    let passwordRegex = RegExp('/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z0-9]{8,}$/');
    if (!passwordRegex.test(password)) {
        errors.push('Parool ei vasta nõuetele');
    }
    User.find({ email: email }).exec((err, result) => {
        if (result) {
            errors.push('Selline email on juba kasutusel');
        }
    });

    if (errors.lenght > 0) {
        console.log(errors)
        //TODO: saada ka sõnum kasutajale
        res.redirect('/users/login');

    }

    else {
        let newUser = new User({
            username: username,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password
        });
        //krüpteerime parooli
        Bcrypt.genSalt(10, (err, salt) => {
            Bcrypt.hash(newUser.password, salt, (err, hash) => {
                if (err) throw err;
                newUser.password = hash;
                newUser.save().then(user => {
                    console.log('INSERT - User -' + user._id);
                    res.redirect('/users/login');
                }).catch(err => {
                    console.log('ERROR - ' + err);
                });
            });
        });

      }
    }
