const http = require('http');
const Express = require('express');  //objektid suure tähega!

const hostname = '127.0.0.1';
const port = 80;

const server = http.createServer((req,res) => {
    res.statusCode = 200; //200 täh. edukas, status koodid. 
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World!\n');
});

server.listen(port, hostname, () => {
    console.log('Server running at http://'+ hostname +':'+ port +'/'); //teine võimalus `Server running at http://${hostname}:${port}/`
  });