//impordime sisse vahevara
const Express = require('express');  //objektid suure tähega!
const Session = require('express-session');
const Dotenv = require('dotenv');
const Mongoose = require('mongoose');
const BodyParser = require('body-parser');
const methodOverride = require('method-override');
const Ejs =   require('ejs');
const Passport = require('passport');
require('./config/passport')(Passport);


Dotenv.config();

//importime routereid
const mainRouter = require('./routes/main');
const articleRouter = require('./routes/article');
const userRouter = require('./routes/user');

//paneme kokku rakenduse
const app = Express();
app.use(Express.static('public'));
app.use(BodyParser.urlencoded({extended:false}));
app.use(BodyParser.json());
app.use(methodOverride('_method'));
//seadistame frontend vadete mootori
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', Ejs.renderFile);

//console.log("db user on" + process.env.DBUSER)
// andmeaasi ühendamine
//const MongoClient = require('mongodb').MongoClient;
//const uri = `mongodb+srv://${process.env.DBUSER}:${process.env.DBPASSWORD}@cluster0-7fsav.azure.mongodb.net/nodenews?retryWrites=true&w=majority`;
Mongoose.connect(process.env.DBURI, {useNewUrlParser:true, useUnifiedTopology:true});

//seadistame sessiooni
app.use(Session({
  secret: process.env.SECRET,
  resave: true,
  saveUninitialized: true
}));
app.use(Passport.initialize());
app.use(Passport.session());

//lisab sisselogitud kasutaja andmed alati EJS failidele kättesaadavaks
app.use(function(req,res,next){
  res.locals.user = req.user;
  next();
});

/**const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect(err => {
  const collection = client.db("test").collection("devices");
  // perform actions on the collection object
  client.db('nodenews').collection('articles').findOne({},function(err,res){
  console.log(res);
  });
  client.close();
});*/ 

//proovime mongoose-i

//let db = Mongoose.connection;
//db.on('error', console.error.bind(console,'connection error;'));

//const Cat = Mongoose.model('Cat',{name: String});

//const kitty = new Cat({name: 'Miisu'});
//kitty.save().then(() => console.log('meow')); 


//lisame rakendusele kõik routerid
app.use('/', mainRouter);
app.use('/articles', articleRouter);
app.use('/users', userRouter);

app.use(function(req,res,next){
  res.status(404);
  res.render('404.ejs');
});

//käivitame HTTP serveri
app.listen(process.env.PORT, process.env.HOSTNAME, () => {
    console.log('Server running at http://'+ process.env.HOSTNAME +':'+ process.env.PORT +'/');
});
  //teine võimalus `Server running at http://${hostname}:${port}/`